package cn.itcast.feign.fallback;

import cn.itcast.feign.client.UserClient;
import cn.itcast.feign.pojo.User;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class UserClientFallbackFactory implements FallbackFactory<UserClient> {
    @Override
    public UserClient create(Throwable throwable) {
        UserClient userClient = new UserClient() {
            @Override
            public User findById(Long id) {
                User user = new User();
                user.setUsername("降解的user对象");
                return user;
            }
        };


        return userClient ;
    }
}
